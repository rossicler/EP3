class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :Group
      t.string :url
      t.string :nameurl

      t.timestamps
    end
  end
end
