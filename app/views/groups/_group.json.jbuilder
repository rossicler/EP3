json.extract! group, :id, :Group, :url, :nameurl, :created_at, :updated_at
json.url group_url(group, format: :json)
