Rails.application.routes.draw do
  get 'about' => 'about#index'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root 'groups#index'
  resources :groups
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
